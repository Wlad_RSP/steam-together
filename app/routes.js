var APIKey = require('../env').steamAPIKey;
var steamAPIUrl = "http://api.steampowered.com/";
var request = require('request');	
var async = require('async');
var _ = require('lodash');

var userInfoURL = "ISteamUser/GetPlayerSummaries/v0002/";
var ownedGamesURL = "IPlayerService/GetOwnedGames/v0001/";
var vanityURL = "ISteamUser/ResolveVanityURL/v0001/";
var _ = require('lodash');
module.exports = function(app) {

	//Route qui permet de récupérer les informations de l'utilisateur passé en form.
  app.get('/user-info', function(req, res) {
  	var steamid = req.query.steamid;

  	resolveVanityURL( steamid, function(userid){

  		var URLdata = '?key=' + APIKey + '&steamid=' + userid + '&format=json';
  		var URLdata2 = '?key=' + APIKey + '&steamids=' + userid + '&format=json';

  		//Le async permet d'executer la requête pour récupérer les users et les games en meme temps, de manière asynchrone. 
  		//Le callback sera executé lorsque toutes les fonctions seront terminées
  		async.parallel({
		  	user: function(cb){
		  		request(steamAPIUrl + userInfoURL + URLdata2, function (error, response, body) {
				  if (!error && response.statusCode == 200) {
				  	cb(null, JSON.parse(body).response.players[0]);
				  } else {
				  	cb(null, null);
				  }
				})
		  	},
			userGames: function(cb){
				request(steamAPIUrl + ownedGamesURL + URLdata, function (error, response, body) {
				  if (!error && response.statusCode == 200) {
				  	cb(null, JSON.parse(body).response);
				  } else {
				  	cb(null, null);
				  }
				})
			}
		},function(err, userInfos){
			res.send(userInfos);
		})
	});
  });
	//Cette route prend un tableau de steamid dans le req et rend un JSON contenant leurs informations
	//exemple d'URL : http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key=FB4467B73B9012463F2594FEBABCAF47&steamids=[76561197960435530,76561197979193613,76561197988918327]&format=json
  app.get('/friends-info', function(req, res) {
  	var userid = req.query.steamid;
	var URLdata2 = '?key=' + APIKey + '&steamids=' + userid + '&format=json';
	  		request(steamAPIUrl + userInfoURL + URLdata2, function (error, response, body) {
			  if (!error && response.statusCode == 200){
			  	res.send(JSON.parse(body).response.players);
			  }
			});
  });
	
	//cette route permet de récupérer un tableau de joueurs en lui passant un ID de joueur.
    app.get('/friends', function(req, res) {
	var steamid = req.query.steamid;

	var friendsIds;
	var userFriends = "ISteamUser/GetFriendList/v0001/";
	var userFriendsProfiles = "ISteamUser/GetPlayerSummaries/v0002/?key=" + APIKey + "&steamids=";
	//http://api.steampowered.com/ISteamUser/GetFriendList/v0001/?key=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX&steamid=76561197960435530&relationship=friend
		resolveVanityURL( steamid, function(userid){

			var URLdata = '?key=' + APIKey + '&steamid=' + userid + '&relationship=friend&format=json';
			
		  	request(steamAPIUrl + userFriends + URLdata, function (error, response, body) {
			  if (!error && response.statusCode == 200) {
			  	var steamids = _.map(JSON.parse(body).friendslist.friends, function(friend) {
			  		return friend.steamid;
			  	});
			  	request([steamAPIUrl, userFriendsProfiles, steamids].join(''), function(error, response, body) {
			  		res.send(_.get(JSON.parse(body), 'response.players', []));
			  	});
			  	//res.send(body);
			  }
			});
		});
  });

	app.get('/common-games', function(req, res) {

	  	var steamids = req.query.steamids.split(",");

		var allPlayersGames = []

		function inArray(needle, haystack) {
		    var length = haystack.length;
		    for(var i = 0; i < length; i++) {
		        if(haystack[i] == needle) return true;
		    }
		    return false;
		}

		async.map(steamids, function (userid, cb) {
		  var URLForGames = '?key=' + APIKey + '&steamid=' + userid + '&include_appinfo=1&format=json';
		  request(steamAPIUrl + ownedGamesURL + URLForGames, function (error, response, body) {
		    if (!error && response.statusCode == 200) {
		      cb(null, JSON.parse(body).response.games);
		    }
		  });
		}, function (err, commonAppIds)  {
			var filteredArray = [];
			for (var i = 0; i < commonAppIds.length ; i++){
				if(i == 0){
					filteredArray = commonAppIds[i];
				}
				filteredArray = _.intersectionBy(filteredArray,commonAppIds[i],'appid');
			}
			async.map(filteredArray, function (game, cb) {
			  var URLForGames = 'http://store.steampowered.com/api/appdetails?appids=' + game.appid ;
			  request(URLForGames, function (error, response, body) {
			    if (!error && response.statusCode == 200) {
			    	var response = JSON.parse(body)[game.appid];
			    	if(response.success == true){
				      	var categories = _.intersectionBy(response.data.categories,[{'id':1},{'id':9},{'id':20},{'id':27}],'id');
				      	if(categories.length > 0){
				      		response['categoriesId'] = [];
				      		for(category of categories){
				      			response['categoriesId'].push(category.id);
				      		}
				      		return cb(null,response);
				      	} else {
				      		return cb(null, null);
				      	}
			    	} else {
			    		return cb(null, null);
			    	}
			    }
			  });
			}, function (err, games)  {
				games = _.remove(games, function(game) {
				  	if(game != null){
				  		return game.data.type =='game';
					}
				});
				return res.send(games);
			});
		});
	});

  app.get('/my-games', function(req, res){
  	var steamid = req.query.steamid;

		resolveVanityURL( steamid, function(userid){

	  		var URLdata = '?key=' + APIKey + '&steamid=' + userid + '&format=json';
			
		  	request(steamAPIUrl + ownedGamesURL + URLdata, function (error, response, body) {
			  if (!error && response.statusCode == 200) {
			  	res.send(body);
			  }
			});
		});
	});

    app.get('/common-games', function(req, res) {
	// steamids est un tableau, on va créer un user avec sa fonction request pour chaque index du tableau.
	// comme ca quand on le passera a async, ce sera un objet contenant tous les users.
	  	var steamids = req.query.steamids.split(",");

		async.map(steamids, function (userid, cb) {
		  var URLForGames = '?key=' + APIKey + '&steamid=' + userid + '&format=json';
		  request(steamAPIUrl + ownedGamesURL + URLForGames, function (error, response, body) {
		    if (!error && response.statusCode == 200) {
		      return cb(null, JSON.parse(body).response.games);
		    }
		  });
		}, function (err, allPlayersGames) {

			var tab = [];

			for( i in allPlayersGames){
				for(y in allPlayersGames[i]){
			  	// console.log('appid : ', allPlayersGames[i][y].appid);
			  }
			}

			// var result = allPlayersGames.shift().filter(function(appid) {
			// 	// console.log('v : ',v);
			//     return allPlayersGames.every(function(a) {
			// 		// console.log('a : ',a);
			//         return a.indexOf(appid) !== -1;
			//     });
			// });

			var result = _(allPlayersGames)
			  .flatten()
			  .uniqBy('appid')
			  .filter(function(game) {
			    return allPlayersGames.every(function(playersGames){
			      return  _.find(playersGames, {
			        "appid": game.appid,
			        "name": game.name
			      });
			    });
			  }).value();


		  res.send(result);
			
		});

	});

}

//------------------------------------------------------------//

//La fonction "resolveVanityURL permet d'avoir en entrée le nom de l'utilisateur et en sortie son ID sous la forme XXXXXXXXXXXXXXXXX
var resolveVanityURL = function (username, f){
	request(steamAPIUrl + vanityURL + "?key=" + APIKey + "&vanityurl=" + username
		, function (error, response, body) {
			var jsonResponse = JSON.parse(response.body);

			if (!error && jsonResponse.response.success == 1){
	  			return f(jsonResponse.response.steamid);
	  		}
			return f(username);
  	});
}