var express    = require('express');
var port       = require('./env').httpServer.port;
var bodyParser = require('body-parser');
var app        = express();

app.configure = function() {
  app.use(express.static('public'));
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({
    extended: false
  }));
};

app.configure();

require('./app/routes')(app);

var server = app.listen(port, function() {
    console.log('Listening on port %d', server.address().port);
});