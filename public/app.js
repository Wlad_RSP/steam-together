var serverURL = '/';
var app = angular.module('SteamTogether', []);

app.controller('ShowFriendsCtrl', ["$scope", "$http", "$timeout", function($scope, $http, $timeout){
	$scope.userInfos = false;
	$scope.loader = false;
	$scope.friendsList = [];
	$scope.games = [];
	$scope.error = false;
	var $friendsSelected;

	$scope.getUserInfo = function() {
		if($scope.username != undefined){
			$http({
		       url : serverURL + 'user-info?steamid=' + $scope.username,
		       method : 'GET'
		    }).then(function successCallback(response) {
		    	$scope.userInfos = false;
		    	$scope.friendsSelected = [];
				$scope.games = [];
				$scope.friendsList = [];
				if(response.data.userGames != null){
					$scope.error = false;
					$scope.userInfos = response.data;
					$scope.userInfos['totalTime'] = 0;
					$scope.userInfos.userGames.games.forEach(function(curVal, index, array){
						$scope.userInfos.totalTime = $scope.userInfos.totalTime + curVal.playtime_forever;
					});
					$scope.getFriends();
					return $scope.userInfos;
				} else {
					$scope.error = 'Sorry but it seems like your Steam Community ID does not exist. Make sure you have it activated in your steam account settings. Need help ? Google is your friend ^^';
				}
			}, function errorCallback(response) {
				$("#userFriends").text('Sorry, an error occured.');
			    // called asynchronously if an error occurs
			    // or server returns response with an error status.
			});
		} else {
			$scope.error = 'uhhh sorry, but it seems like you have forgetten something ^';
		}
	}

	$scope.compare = function() {

		friendsSelected = _.filter($scope.friendsList, function(friend) {
			return friend.selected;
		});

		var steamids = _.map(friendsSelected, function(selected) {
			return selected.friend.steamid;
		});
		if(steamids == ''){
			steamids = $scope.userInfos.user.steamid;
		} else {
			steamids = steamids.join(',');
		}
		$http({
		  method: 'GET',
		  url: serverURL + 'common-games?steamids=' + steamids
		}).then(function successCallback(response) {
			$scope.error = false;
			$scope.games = response.data;
			$scope.friendsList = [];
			$scope.friendsSelected = friendsSelected;
			return $scope.games;
		}, function errorCallback(response) {
			$("#userFriends").text('Sorry, an error occured. Refresh the page if the error persists!');
		    // called asynchronously if an error occurs
		    // or server returns response with an error status.
		});
	};
	$scope.selectedCount = function() {
		var count = 0;
		 _.map($scope.friendsList, function(friend) {
			if(friend.selected){
				count++;
			}
		});
		return count;
	};
    $scope.selected = [1];
    $scope.pushed = {'1':true,'27':false,'9':false,'20':false}; 
    $scope.toggleFilter = function(id){
    	if($scope.selected.indexOf(id) < 0){
    		$scope.selected.push(id);
    		$scope.pushed[id] = true;
    	} else {
    		var index = $scope.selected.indexOf(id);
    		$scope.selected.splice(index,1);
    		$scope.pushed[id] = false;
    	}
    }

	$scope.select = function(friendData) {
		if($scope.selectedCount() < 8){
			friendData.selected = !friendData.selected;
		} else if (friendData.selected) {
			friendData.selected = !friendData.selected;
		}
	};
	$scope.getFriends = function(){
		// Simple GET request example:
		$scope.loader = true;
		$http({
		  method: 'GET',
		  url: serverURL + 'friends?steamid=' + $scope.username
		}).then(function successCallback(response) {
			$scope.games = [];
			$scope.friendsList = [];
			$scope.friendsList = _.map(response.data, function(friend) {
				var tempFriend  = friend;
				tempFriend.lastlogoff = new Date(friend.lastlogoff*1000);
				tempFriend.lastlogoff = tempFriend.lastlogoff.toISOString();
				$scope.loader = false;
				return {
					selected: false,
					friend: tempFriend
				}
			});

			$timeout(function() {
				$('time.timeago').timeago();
			}, 100);
		}, function errorCallback(response) {
			$("#userFriends").text('Sorry, an error occured.');
		    // called asynchronously if an error occurs
		    // or server returns response with an error status.
		});
	}
}]);

app.filter('time', function() {
    
    var conversions = {
      'mm': angular.identity,
      'hh': function(value) { return value * 60; },
      'jj': function(value) { return value * 1440; }
    };
    
    var padding = function(value, length) {
      var zeroes = length - ('' + (value)).length,
          pad = '';
      while(zeroes-- > 0) pad += '0';
      return pad + value;
    };
    
    return function(value, unit, format, isPadded) {
      var totalMinutes = conversions[unit || 'mm'](value),
          jj = Math.floor(totalMinutes / 1440),
          hh = Math.floor((totalMinutes % 1440) / 60),
          mm = totalMinutes % 60;
      
      format = format || 'jjD hhH mmM';
      isPadded = angular.isDefined(isPadded)? isPadded: true;
      jj = isPadded? padding(jj, 2): jj;
      hh = isPadded? padding(hh, 2): hh;
      mm = isPadded? padding(mm, 2): mm;

      return format.replace(/jj/, jj).replace(/hh/, hh).replace(/mm/, mm);
    };
  });

app.filter('matchesAll', function() {
	    return function(items, relevant, property) {
	        if(!(relevant && relevant.length)){
	            return items; // No categories to compare with, return everything
	        }

	        property = property || 'categoriesId'; // By default look at categories

	        return items.filter(function(item) {
	            var itemProps = item[property];
	            return relevant.every(function(relevantCategory){
	                return itemProps.indexOf(relevantCategory) !== -1;
	            });
	        });
	    };
	});



